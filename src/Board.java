import java.util.Scanner;

public class Board {
	char board[][] = {

			{'-','-','-'},
			{'-','-','-'},
			{'-','-','-'}
	};
	private Player x;
	private Player o;
	private Player winner;
	private Player current;
	private int turnCount;
	
	
	static Scanner kb = new Scanner(System.in);
	
	public Board (Player x, Player o) {
		this.x = x;
		this.o = o;
		current =x ;
		winner = null;
		turnCount = 0;
	}
	
	public char[][] getBoard() {
		return board;
	}
	
	public Player getCurrent() {
		return current;
	}
	

	public Player getWinner() {
		return winner;
	}

	
	
	public boolean setTable(int row,int col) {
		if(board[row][col] == '-') {
			board[row][col] = current.getName();
			return true;
		}
		return false;
	}
	
	public void switchPlayer() {
		if (current == x) {
			current = o;
		} else
			current = x;
	}
	
	public boolean isFull(int row, int col) {
		if (board[row - 1][col - 1] == 'X' || board[row - 1][col - 1] == 'O') {
			return true;
		}
		return false;
	}

	public boolean OutOfBoard(int row, int col) {
		if (row > 3 || col > 3) {
			System.err.println("There is no row or column");
			return true;

		}
		return false;
	}

	public void showTurn() {
		System.out.println(current.getName() + " turn...");
	}
	
	public void showBoard() {
		System.out.println("  1 2 3");
		for(int i = 0 ; i < board.length;i++) {
			System.out.print(i+1);
			for(int k = 0; k < board[i].length;k++) {
				System.out.print(" "+board[i][k]);
			}
			System.out.println();
		}
	}
	
	public void check() {
		while (winner == null) {
			showTurn();
			System.out.println(current.getName() + "(R,C):");
			int row = kb.nextInt();
			int col = kb.nextInt();
			if (OutOfBoard(row, col) == true) {
				System.out.println("You've inputed place, which is out of the board!\nTry again!");

			} else {
				if (isFull(row, col)) {
					System.err.println("Table is  not empty");

				} else {
					board[row - 1][col - 1] = getCurrent().getName();
					showBoard();
					if (Winner() == false) {
						if (current == x) {
							winner = x;
							System.out.println("the winner is " + getCurrent().getName());
						} else {
							winner = o;
							System.out.println("the winner is " + getCurrent().getName());
						}
					} else {
						switchPlayer();
						turnCount++;
					}
				}

			}
			if (turnCount > 8) {
				System.out.println("Draw!");
				break;
			}

		}
		if (getWinner() == x) {
			x.win();
			o.lose();
		} else if (getWinner() == o) {
			o.win();
			x.lose();
		} else {
			o.draw();
			x.draw();
		}
	}

	
	public boolean Winner() {

		if (board[0][0] != '-' && board[0][0] == board[0][1] && board[0][0] == board[0][2])
			return false;
		if (board[1][0] != '-' && board[1][0] == board[1][1] && board[1][0] == board[1][2])
			return false;
		if (board[2][0] != '-' && board[2][0] == board[2][1] && board[2][0] == board[2][2])
			return false;
		if (board[0][0] != '-' && board[0][0] == board[1][0] && board[0][0] == board[2][0])
			return false;
		if (board[0][1] != '-' && board[0][1] == board[1][1] && board[0][1] == board[2][1])
			return false;
		if (board[0][2] != '-' && board[0][2] == board[1][2] && board[0][2] == board[2][2])
			return false;
		if (board[0][0] != '-' && board[0][0] == board[1][1] && board[0][0] == board[2][2])
			return false;
		if (board[0][2] != '-' && board[0][2] == board[1][1] && board[0][2] == board[2][0])
			return false;

		return true;
	}


}
