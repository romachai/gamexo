import java.util.Scanner;

public class Game {
	private Board b;
	private Player x ;
	private Player o ;
	
	static Scanner kb = new Scanner(System.in);
	
	public Game () {
		o = new Player('O');
		x = new Player('X');
		b = new Board(x,o);
	}
	
	public void Play() {
		showWelcome();
		b.showBoard();
		b.check();
		choice();

	}
	
	public void showWelcome() {
		System.out.println("Welcome to GameXO");
	}
			
	private void choice() {
		System.out.println("Do you want to play again? <yes/no>:");
		String ch = kb.next();
		if(ch.equalsIgnoreCase("yes")) {
			b = new Board(x, o);
			Play();
		}else if(ch.equalsIgnoreCase("no")) {
			showStat();
			showBye();
		}
	}

	
	public void showBye() {
		System.out.println("Thank you for playing.");
	}
	
	public void showStat() {
		System.out.println(x.getName() + " (W:D:L):(" + x.getWin() + ":" + x.getDraw() + ":" + x.getLose() + ")");
		System.out.println(o.getName() + " (W:D:L):(" + o.getWin() + ":" + o.getDraw() + ":" + o.getLose() + ")");
	}
}